# Local JMeter setup with InfluxDB
This repository contains some Ansible scripts to support the setup of a
virtual machine in Virtual Box collecting JMeter sample data.

## Background
JMeter is a very flexible tool when it comes to setup and run load and
performance test. Usually running those tests will result in a larger amount of data. JMeter provides only very limited options to analyze and visualize that data. But JMeter provides a [built-in listener](https://jmeter.apache.org/usermanual/component_reference.html#Backend_Listener) to send sample data to more powerful tools. One of those tools is [InfluxDB](https://www.influxdata.com/products/influxdb/) - a time series database.
The actual version supports creating complex database queries. It also has a web ui with several types of charts for visualizing the query results.

To keep the complexity low, it is better to install the InfluxDB into its own virtual machine. This way it is possible to delete the whole database when no longer needed.

## Prerequisites
To create the virtual machine providing InfluxDB you have to install the following tools on your local system:

* [Vagrant](https://www.vagrantup.com/) 2.2.6
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html) 2.9.6
* [Virtualbox](https://www.virtualbox.org/) 6.1.26

The setup was developed using those versions. Newer versions of the tools above might require adaption in the config files.

You may also [download JMeter](https://jmeter.apache.org/download_jmeter.cgi) to create your own tests. If you want to execute the template script `jmeter sample setup.jmx` you have to download the [plugin manager for JMeter](https://jmeter-plugins.org/install/Install/) and copy the file to the `ext/lib` directory of JMeter.

## Installation
When your local machine does meet all the prerequisites the next step is to clone this repository. You may then open a terminal at the location of this README and enter the command `vagrant up`. That's it. Vagrant will now create a virtual machine install and configure InfluxDB on that machine. The configuration of InfluxDB will setup an initial user and create a bucket for the JMeter sample data.

## Usage

### Collect sample data with JMeter
This repository contains the JMeter script `jmeter sample setup.jmx` The installation of the virtual machine will also insert the actual API token from InfluxDB to the configuration of the [Backend Listener](https://jmeter.apache.org/usermanual/component_reference.html#Backend_Listener).

The JMeter script template is using a [Dummy Sampler](https://jmeter-plugins.org/wiki/DummySampler/) to generate random data. When you run the script InfluxDB will alread collect this data so that you can play around with queries and try to setup a dashboard.

### Analyze test results with InfluxDB queries
#### InfluxDB Gui
The installation will forward port 8086 to locahost. This allows you to open the InfluxDB Gui by open the url [http://localhost:8086](http://localhost:8086) in your browser. Use jmeter:adminadmin as login.

#### Command line interface
When you connect to the virtual machine using `vagrant ssh` you can [use the InfluxDB cli](https://docs.influxdata.com/influxdb/v2.1/reference/cli/influx/query/) `influx` to interact with the database. Installation did create a default profile so that you don't have to use the api token as parameter for every query or command.
